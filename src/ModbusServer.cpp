#include "ModbusServer.h"
static uint8_t auchCRCHi[] =  // 256 bytes
{  
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0,
    0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
    0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1,
    0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1,
    0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0,
    0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40,
    0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1,
    0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0,
    0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40,
    0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
    0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0,
    0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
    0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
    0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
    0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40,
    0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1,
    0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0,
    0x80, 0x41, 0x00, 0xC1, 0x81, 0x40
} ;

static uint8_t auchCRCLo[] =  // 256 bytes
{
    0x00, 0xC0, 0xC1, 0x01, 0xC3, 0x03, 0x02, 0xC2, 0xC6, 0x06,
    0x07, 0xC7, 0x05, 0xC5, 0xC4, 0x04, 0xCC, 0x0C, 0x0D, 0xCD,
    0x0F, 0xCF, 0xCE, 0x0E, 0x0A, 0xCA, 0xCB, 0x0B, 0xC9, 0x09,
    0x08, 0xC8, 0xD8, 0x18, 0x19, 0xD9, 0x1B, 0xDB, 0xDA, 0x1A,
    0x1E, 0xDE, 0xDF, 0x1F, 0xDD, 0x1D, 0x1C, 0xDC, 0x14, 0xD4,
    0xD5, 0x15, 0xD7, 0x17, 0x16, 0xD6, 0xD2, 0x12, 0x13, 0xD3,
    0x11, 0xD1, 0xD0, 0x10, 0xF0, 0x30, 0x31, 0xF1, 0x33, 0xF3,
    0xF2, 0x32, 0x36, 0xF6, 0xF7, 0x37, 0xF5, 0x35, 0x34, 0xF4,
    0x3C, 0xFC, 0xFD, 0x3D, 0xFF, 0x3F, 0x3E, 0xFE, 0xFA, 0x3A,
    0x3B, 0xFB, 0x39, 0xF9, 0xF8, 0x38, 0x28, 0xE8, 0xE9, 0x29,
    0xEB, 0x2B, 0x2A, 0xEA, 0xEE, 0x2E, 0x2F, 0xEF, 0x2D, 0xED,
    0xEC, 0x2C, 0xE4, 0x24, 0x25, 0xE5, 0x27, 0xE7, 0xE6, 0x26,
    0x22, 0xE2, 0xE3, 0x23, 0xE1, 0x21, 0x20, 0xE0, 0xA0, 0x60,
    0x61, 0xA1, 0x63, 0xA3, 0xA2, 0x62, 0x66, 0xA6, 0xA7, 0x67,
    0xA5, 0x65, 0x64, 0xA4, 0x6C, 0xAC, 0xAD, 0x6D, 0xAF, 0x6F,
    0x6E, 0xAE, 0xAA, 0x6A, 0x6B, 0xAB, 0x69, 0xA9, 0xA8, 0x68,
    0x78, 0xB8, 0xB9, 0x79, 0xBB, 0x7B, 0x7A, 0xBA, 0xBE, 0x7E,
    0x7F, 0xBF, 0x7D, 0xBD, 0xBC, 0x7C, 0xB4, 0x74, 0x75, 0xB5,
    0x77, 0xB7, 0xB6, 0x76, 0x72, 0xB2, 0xB3, 0x73, 0xB1, 0x71,
    0x70, 0xB0, 0x50, 0x90, 0x91, 0x51, 0x93, 0x53, 0x52, 0x92,
    0x96, 0x56, 0x57, 0x97, 0x55, 0x95, 0x94, 0x54, 0x9C, 0x5C,
    0x5D, 0x9D, 0x5F, 0x9F, 0x9E, 0x5E, 0x5A, 0x9A, 0x9B, 0x5B,
    0x99, 0x59, 0x58, 0x98, 0x88, 0x48, 0x49, 0x89, 0x4B, 0x8B,
    0x8A, 0x4A, 0x4E, 0x8E, 0x8F, 0x4F, 0x8D, 0x4D, 0x4C, 0x8C,
    0x44, 0x84, 0x85, 0x45, 0x87, 0x47, 0x46, 0x86, 0x82, 0x42,
    0x43, 0x83, 0x41, 0x81, 0x80, 0x40
};

static void mbs_rtu_crc16(uint8_t* data, uint16_t length, uint8_t *crc_h, uint8_t *crc_l)
{
    uint8_t uchCRCHi = 0xFF; 
    uint8_t uchCRCLo = 0xFF; 
    uint16_t uIndex; 
    while (length--)
    {
        uIndex = uchCRCHi ^ *data++ ;
        uchCRCHi = uchCRCLo ^ auchCRCHi[uIndex];
        uchCRCLo = auchCRCLo[uIndex];
    }
    *crc_h = uchCRCHi;
    *crc_l = uchCRCLo;
    //return (uchCRCHi << 8 | uchCRCLo);
}

ModbusServer::ModbusServer()
{
    for(int i = 0; i < 128; i++)
    {
        _func[i] = NULL;
    }
}

void ModbusServer::SetSlaveAddress(uint8_t addr)
{
    _slaveAddr = addr;
}

void ModbusServer::RegisterFunction(uint8_t code, mbs_func_t f)
{
    _func[code] = f;
}

void ModbusServer::SetDataModel(ModbusData* data)
{
    _dataModel = data;
}

int ModbusServer::Serve(uint8_t* data_in, uint16_t length, uint8_t* data_out)
{
    uint8_t slaveAddress = data_in[0];
    if((slaveAddress == _slaveAddr) && (length >= 4))
    {
        uint8_t crc_h, crc_l;
        mbs_rtu_crc16(data_in, length - 2, &crc_h, &crc_l);
        if((crc_h == data_in[length - 2]) && (crc_l == data_in[length - 1])) // check CRC
        {
            uint8_t functionCode = data_in[1];
            mbs_func_t f = _func[functionCode];
            if(f != NULL)
            {
                uint16_t length;
                mbs_err_t err = f(&data_in[2], _dataModel, &data_out[2], &length);
                if(err == MBS_ERR_OK)
                {
                    data_out[0] = slaveAddress;
                    data_out[1] = functionCode;
                    mbs_rtu_crc16(data_out, length + 2, &crc_h, &crc_l);
                    data_out[length + 2] = crc_h;
                    data_out[length + 3] = crc_l;
                    return length + 4;
                }
                else
                {
                    data_out[0] = slaveAddress;
                    data_out[1] = MBS_ERR_MASK | functionCode;
                    data_out[2] = err;
                    mbs_rtu_crc16(data_out, 3, &crc_h, &crc_l);
                    data_out[3] = crc_h;
                    data_out[4] = crc_l;
                }
            }
            else // do not support function code
            {
                data_out[0] = slaveAddress;
                data_out[1] = MBS_ERR_MASK | functionCode;
                data_out[2] = MBS_ERR_FUNCTION_CODE;
                mbs_rtu_crc16(data_out, 3, &crc_h, &crc_l);
                data_out[3] = crc_h;
                data_out[4] = crc_l;
            }
            return 5;
        }
    }
    return -1;
}

mbs_err_t ModbusServer::func_0x01_std(uint8_t* pdu, ModbusData* model, uint8_t* data_out, uint16_t* length)
{
    uint16_t startAddr = (pdu[0] << 8) | pdu[1];
    uint16_t quantity = (pdu[2] << 8) | pdu[3];
    mbs_err_t result = model->ReadCoil(data_out, startAddr, quantity);
    if(result == MBS_ERR_OK)
    {
        *length = data_out[0] + 1;
    }
    else
    {
        *length = 0;
    }
    return result;
}

mbs_err_t ModbusServer::func_0x02_std(uint8_t* pdu, ModbusData* model, uint8_t* data_out, uint16_t* length)
{
    uint16_t startAddr = (pdu[0] << 8) | pdu[1];
    uint16_t quantity = (pdu[2] << 8) | pdu[3];
    mbs_err_t result = model->ReadInputStatus(data_out, startAddr, quantity);
    if(result == MBS_ERR_OK)
    {
        *length = data_out[0] + 1;
    }
    else
    {
        *length = 0;
    }
    return result;
}

mbs_err_t ModbusServer::func_0x03_std(uint8_t* pdu, ModbusData* model, uint8_t* data_out, uint16_t* length)
{
    uint16_t startAddr = (pdu[0] << 8) | pdu[1];
    uint16_t quantity = (pdu[2] << 8) | pdu[3];
    mbs_err_t result = model->ReadHoldingRegister(data_out, startAddr, quantity);
    if(result == MBS_ERR_OK)
    {
        *length = data_out[0] + 1;
    }
    else
    {
        *length = 0;
    }
    return result;
}

mbs_err_t ModbusServer::func_0x04_std(uint8_t* pdu, ModbusData* model, uint8_t* data_out, uint16_t* length)
{
    uint16_t startAddr = (pdu[0] << 8) | pdu[1];
    uint16_t quantity = (pdu[2] << 8) | pdu[3];
    mbs_err_t result = model->ReadInputRegister(data_out, startAddr, quantity);
    if(result == MBS_ERR_OK)
    {
        *length = data_out[0] + 1;
    }
    else
    {
        *length = 0;
    }
    return result;
}

mbs_err_t ModbusServer::func_0x05_std(uint8_t* pdu, ModbusData* model, uint8_t* data_out, uint16_t* length)
{
    uint16_t startAddr = (pdu[0] << 8) | pdu[1];
    mbs_err_t result = model->WriteCoil(&pdu[2], startAddr);
    if(result == MBS_ERR_OK)
    {
        *length = 4;
        data_out[0] = pdu[0];
        data_out[1] = pdu[1];
        data_out[2] = pdu[2];
        data_out[3] = pdu[3];
    }
    else
    {
        *length = 0;
    }
    return result;
}

mbs_err_t ModbusServer::func_0x06_std(uint8_t* pdu, ModbusData* model, uint8_t* data_out, uint16_t* length)
{
    uint16_t startAddr = (pdu[0] << 8) | pdu[1];
    mbs_err_t result = model->WriteHoldingRegister(&pdu[2], startAddr);
    if(result == MBS_ERR_OK)
    {
        *length = 4;
        data_out[0] = pdu[0];
        data_out[1] = pdu[1];
        data_out[2] = pdu[2];
        data_out[3] = pdu[3];
    }
    else
    {
        *length = 0;
    }
    return result;
}

mbs_err_t ModbusServer::func_0x0f_std(uint8_t* pdu, ModbusData* model, uint8_t* data_out, uint16_t* length)
{
    uint16_t startAddr = (pdu[0] << 8) | pdu[1];
    uint16_t quantity = (pdu[2] << 8) | pdu[3];
    mbs_err_t result = model->WriteCoil(&pdu[4], startAddr, quantity);
    if(result == MBS_ERR_OK)
    {
        *length = 4;
        data_out[0] = pdu[0];
        data_out[1] = pdu[1];
        data_out[2] = pdu[2];
        data_out[3] = pdu[3];
    }
    else
    {
        *length = 0;
    }
    return result;
}


mbs_err_t ModbusServer::func_0x10_std(uint8_t* pdu, ModbusData* model, uint8_t* data_out, uint16_t* length)
{
    uint16_t startAddr = (pdu[0] << 8) | pdu[1];
    uint16_t quantity = (pdu[2] << 8) | pdu[3];
    mbs_err_t result = model->WriteHoldingRegister(&pdu[4], startAddr, quantity);
    if(result == MBS_ERR_OK)
    {
        *length = 4;
        data_out[0] = pdu[0];
        data_out[1] = pdu[1];
        data_out[2] = pdu[2];
        data_out[3] = pdu[3];
    }
    else
    {
        *length = 0;
    }
    return result;
}

mbs_err_t ModbusServer::func_0x17_std(uint8_t* pdu, ModbusData* model, uint8_t* data_out, uint16_t* length)
{
    uint16_t readStartAddr = (pdu[0] << 8) | pdu[1];
    uint16_t readQuantity = (pdu[2] << 8) | pdu[3];
    uint16_t writeStartAddr = (pdu[4] << 8) | pdu[5];
    uint16_t writeQuantity = (pdu[6] << 8) | pdu[7];
    mbs_err_t result = model->ReadWriteHoldingRegister(&pdu[8], readStartAddr, readQuantity, data_out, writeStartAddr, writeQuantity);
    if(result == MBS_ERR_OK)
    {
        *length = data_out[0] + 1;
    }
    else
    {
        *length = 0;
    }
    return result;
}

int ModbusTCPServer::Serve(uint8_t* data_in, uint16_t length, uint8_t* data_out)
{
    if((data_in[2] == 0) && (data_in[3] == 0) && (length >= 8)) // check if protocol id == 0
    {
        uint8_t functionCode = data_in[7];
        mbs_func_t f = _func[functionCode];
        if(f != NULL)
        {
            uint16_t len;
            mbs_err_t err = f(&data_in[8], _dataModel, &data_out[8], &len);
            *(uint32_t*)data_out = *(uint32_t*)data_in; // copy first 4 bytes in MBAP header
            data_out[6] = data_in[6]; // copy  unit id
            if(err == MBS_ERR_OK)
            {
                uint16_t tmp = len + 2;
                data_out[4] = (tmp & 0xff00) >> 8;
                data_out[5] = tmp & 0xff;
                data_out[7] = functionCode;
                return len + 8;
            }
            else
            {
                data_out[4] = 0;
                data_out[5] = 3;
                data_out[7] = MBS_ERR_MASK | functionCode;
                data_out[8] = err;
            }
        }
        else // do not support function code
        {
            data_out[4] = 0;
            data_out[5] = 3;
            data_out[7] = MBS_ERR_MASK | functionCode;
            data_out[8] = MBS_ERR_FUNCTION_CODE;
        }
        return 9;    
    }
    return -1;
}

int ModbusRTUServer::Serve(uint8_t* data_in, uint16_t length, uint8_t* data_out)
{
    uint8_t slaveAddress = data_in[0];
    if((slaveAddress == _slaveAddr) && (length >= 4))
    {
        uint8_t crc_h, crc_l;
        mbs_rtu_crc16(data_in, length - 2, &crc_h, &crc_l);
        if((crc_h == data_in[length - 2]) && (crc_l == data_in[length - 1])) // check CRC
        {
            uint8_t functionCode = data_in[1];
            mbs_func_t f = _func[functionCode];
            if(f != NULL)
            {
                uint16_t len;
                mbs_err_t err = f(&data_in[2], _dataModel, &data_out[2], &len);
                if(err == MBS_ERR_OK)
                {
                    data_out[0] = slaveAddress;
                    data_out[1] = functionCode;
                    mbs_rtu_crc16(data_out, len + 2, &crc_h, &crc_l);
                    data_out[len + 2] = crc_h;
                    data_out[len + 3] = crc_l;
                    return len + 4;
                }
                else
                {
                    data_out[0] = slaveAddress;
                    data_out[1] = MBS_ERR_MASK | functionCode;
                    data_out[2] = err;
                    mbs_rtu_crc16(data_out, 3, &crc_h, &crc_l);
                    data_out[3] = crc_h;
                    data_out[4] = crc_l;
                }
            }
            else // do not support function code
            {
                data_out[0] = slaveAddress;
                data_out[1] = MBS_ERR_MASK | functionCode;
                data_out[2] = MBS_ERR_FUNCTION_CODE;
                mbs_rtu_crc16(data_out, 3, &crc_h, &crc_l);
                data_out[3] = crc_h;
                data_out[4] = crc_l;
            }
            return 5;
        }
    }
    return -1;
}