#include "ModbusData.h"

ModbusData::ModbusData()
{
    _blockCount = 0;
}

void ModbusData::AddBlock(uint16_t addr, uint16_t quantity, void *data, mbs_data_type_t type)
{
    _blocks[_blockCount].addr = addr;
    _blocks[_blockCount].quantity = quantity;
    _blocks[_blockCount].pData = (uint16_t*)data;
    _blocks[_blockCount].type = type;
    _blockCount++;
}

ModbusDataBlock* ModbusData::FindBlock(uint16_t addr, mbs_data_type_t type)
{   
    for(int i = 0; i < _blockCount; i++)
    {
        ModbusDataBlock *bk = &_blocks[i];
        if(bk->type == type)  // check type
        {
            if((bk->addr <= addr) && (bk->addr + bk->quantity) > addr)
            {
                return bk;
            }
        }
    }
    return NULL;
}

mbs_err_t ModbusData::ReadRegister(uint8_t* buffer_out, uint16_t startAddr, uint16_t quantity, mbs_data_type_t type)
{
    if(quantity >= 1 && quantity <= MBS_MAX_REGISTER_READ_QUANTITY)
    {
        ModbusDataBlock* bk = FindBlock(startAddr, type);
        if(bk != NULL)  // check address valid
        {
            if((bk->addr + bk->quantity - startAddr - quantity) >= 0) // check address again
            {
                uint8_t bytecount = quantity << 1;
                buffer_out[0] = bytecount;
                uint16_t baseoff = startAddr - bk->addr;
                uint16_t* dt = (uint16_t*)((uint32_t)(bk->pData) + (baseoff << 1));
                for(uint8_t i = 0; i < bytecount; i = i + 2)
                {
                    uint16_t value = *dt++;
                    buffer_out[i + 1] = (value & 0xFF00) >> 8;
                    buffer_out[i + 2] = (value & 0xFF);        // Modbus Standard is Big-endian
                }
                return MBS_ERR_OK;
            }   
            else
            {
                return MBS_ERR_ADDRESS;
            }
        }
        else
        {
            return MBS_ERR_ADDRESS;
        }
    }
    else
    {
        return MBS_ERR_QUANTITY;
    }
}

mbs_err_t ModbusData::ReadBit(uint8_t* buffer_out, uint16_t startAddr, uint16_t quantity, mbs_data_type_t type)
{
    if(quantity >= 1 && quantity <= MBS_MAX_BIT_READ_QUANTITY)
    {
        ModbusDataBlock* bk = FindBlock(startAddr, type);
        if(bk != NULL)  // check address valid
        {
            if((bk->addr + bk->quantity - startAddr - quantity) >= 0) // check address again
            {
                uint16_t div = quantity / 8;
                uint16_t re = quantity % 8;
                if(re == 0)
                {
                    buffer_out[0] = div;
                }
                else
                {
                    buffer_out[0] = div + 1;
                }
                uint16_t baseoff = startAddr - bk->addr;
                mbs_bit* dt = (mbs_bit*)((uint32_t)(bk->pData) + baseoff * sizeof(mbs_bit));
                for(int i = 1; i <= div; i++)
                {
                    for(uint8_t j = 0x01; j != 0; j = j << 1)
                    {
                        if(*dt)
                        {
                            buffer_out[i] |= j;
                        }
                        dt++;
                    }
                }
                if(re)
                {
                    buffer_out[div + 1] = 0;
                    for(int i = 1, j = 0x01; i <= re; i++, j = j << 1)
                    {
                        if(*dt)
                        {
                            buffer_out[div + 1] |= j;
                        }
                        dt++;
                    }
                }
                return MBS_ERR_OK;
            }   
            else
            {
                return MBS_ERR_ADDRESS;
            }
        }
        else
        {
            return MBS_ERR_ADDRESS;
        }
    }
    else
    {
        return MBS_ERR_QUANTITY;
    }
}

 // Function Code 0x01: Read coils
mbs_err_t ModbusData::ReadCoil(uint8_t* buffer_out, uint16_t startAddr, uint16_t quantity)
{
    return ReadBit(buffer_out, startAddr, quantity, MBS_TYPE_COIL);
}

// Function Code 0x02: Read input status
mbs_err_t ModbusData::ReadInputStatus(uint8_t* buffer_out, uint16_t startAddr, uint16_t quantity)
{
    return ReadBit(buffer_out, startAddr, quantity, MBS_TYPE_INPUT_STATUS);
}

// Function Code 0x03: Read holding registers
mbs_err_t ModbusData::ReadHoldingRegister(uint8_t* buffer_out, uint16_t startAddr, uint16_t quantity)
{
    return ReadRegister(buffer_out, startAddr, quantity, MBS_TYPE_HOLDING_REGISTER);
}

// Function Code 0x04: Read input registers
mbs_err_t ModbusData::ReadInputRegister(uint8_t* buffer_out, uint16_t startAddr, uint16_t quantity)
{
    return ReadRegister(buffer_out, startAddr, quantity, MBS_TYPE_INPUT_REGISTER);
}

// Function Code 0x05: Write single coil
mbs_err_t ModbusData::WriteCoil(const uint8_t* buffer_in, uint16_t startAddr)
{
    ModbusDataBlock* bk = FindBlock(startAddr, MBS_TYPE_COIL);
    if(bk != NULL)  // check address valid
    {
        if(buffer_in[1] == 0)
        {
            uint16_t baseoff = startAddr - bk->addr;
            mbs_bit* dt = (mbs_bit*)((uint32_t)(bk->pData) + baseoff * sizeof(mbs_bit));
            if(buffer_in[0] == 0xFF)
            {
                *dt = 1;
                return MBS_ERR_OK;
            }
            else
            {
                if(buffer_in[0] == 0)
                {
                    *dt = 0;
                    return MBS_ERR_OK;
                }
                else
                {
                    return MBS_ERR_QUANTITY;
                }
            }
        }
        else
        {
            return MBS_ERR_QUANTITY; // error code = 0x03
        }
    }
    else
    {
        return MBS_ERR_ADDRESS;
    }
}

// Function Code 0x06: Write single holding registers
mbs_err_t ModbusData::WriteHoldingRegister(const uint8_t* buffer_in, uint16_t startAddr)
{
    ModbusDataBlock* bk = FindBlock(startAddr, MBS_TYPE_HOLDING_REGISTER);
    if(bk != NULL)  // check address valid
    {
        uint16_t baseoff = startAddr - bk->addr;
        uint16_t* dt = (uint16_t*)((uint32_t)(bk->pData) + (baseoff << 1));
        *dt = (buffer_in[0] << 8) | buffer_in[1];
        return MBS_ERR_OK;
    }
    else
    {
        return MBS_ERR_ADDRESS;
    }
}

// Function Code 0x0F: Write multiple coils
mbs_err_t ModbusData::WriteCoil(const uint8_t* buffer_in, uint16_t startAddr, uint16_t quantity)
{
    uint16_t div = quantity / 8;
    uint16_t re = quantity % 8;
    uint16_t tmp = re > 0 ? div + 1 : div;
    if(tmp == buffer_in[0])
    {
        if((quantity >= 1) && (quantity <= MBS_MAX_BIT_WRITE_QUANTITY))
        {
            ModbusDataBlock* bk = FindBlock(startAddr, MBS_TYPE_COIL);
            if(bk != NULL)  // check address valid
            {
                if((bk->addr + bk->quantity - startAddr - quantity) >= 0) // check address again
                {
                    uint16_t baseoff = startAddr - bk->addr;
                    mbs_bit* dt = (mbs_bit*)((uint32_t)(bk->pData) + baseoff * sizeof(mbs_bit));
                    for(int i = 1; i <= div; i++)
                    {
                        for(uint8_t j = 0x01; j != 0; j = j << 1)
                        {
                            if(buffer_in[i] & j)
                            {
                                *dt = 1;
                            }
                            else
                            {
                                *dt = 0;
                            }
                            dt++;
                        }
                    }
                    if(re)
                    {
                        for(int i = 1, j = 0x01; i <= re; i++, j = j << 1)
                        {
                            if(buffer_in[div + i] & j)
                            {
                                *dt = 1;
                            }
                            else
                            {
                                *dt = 0;
                            }
                            dt++;
                        }
                    }
                    return MBS_ERR_OK;
                }   
                else
                {
                    return MBS_ERR_ADDRESS;
                }
            }
            else
            {
                return MBS_ERR_ADDRESS;
            }
        }
        else
        {
            return MBS_ERR_QUANTITY;
        }
    }
    else
    {
        return MBS_ERR_QUANTITY;
    }
}

// Function Code 0x10: Write multiple holding registers
mbs_err_t ModbusData::WriteHoldingRegister(const uint8_t* buffer_in, uint16_t startAddr, uint16_t quantity)
{
    if(quantity >= 1 && (quantity <= MBS_MAX_REGISTER_WRITE_QUANTITY) && ((quantity << 1) ==  buffer_in[0]))
    {
        ModbusDataBlock* bk = FindBlock(startAddr, MBS_TYPE_HOLDING_REGISTER);
        if(bk != NULL)  // check address valid
        {
            if((bk->addr + bk->quantity - startAddr - quantity) >= 0) // check address again
            {
                // TODO write data from buffer
                uint8_t bytecount = buffer_in[0];
                uint16_t baseoff = startAddr - bk->addr;
                uint16_t* dt = (uint16_t*)((uint32_t)(bk->pData) + (baseoff << 1));
                for(int i = 0; i < bytecount; i = i + 2)
                {
                    uint16_t value = (buffer_in[i + 1] << 8) | buffer_in[i + 2]; // Modbus standard is Big-endian
                    *dt++ = value;
                }
                return MBS_ERR_OK;
            }   
            else
            {
                return MBS_ERR_ADDRESS;
            }
        }
        else
        {
            return MBS_ERR_ADDRESS;
        }
    }
    else
    {
        return MBS_ERR_QUANTITY;
    }
}

// Function Code 0x17: Read/Write multiple registers
mbs_err_t ModbusData::ReadWriteHoldingRegister(uint8_t* buffer_out, 
                                               uint16_t readStartAddr, 
                                               uint16_t readQuantity, 
                                               const uint8_t* buffer_in, 
                                               uint16_t writeStartAddr, 
                                               uint16_t writeQuantity)
{
    if((readQuantity >= 1) 
        && (readQuantity <= MBS_MAX_REGISTER_READ_QUANTITY) 
        && (writeQuantity >= 1)
        && (writeQuantity <= MBS_MAX_REGISTER_READ_WRITE_QUANTITY)
        && ((writeQuantity << 1) == buffer_in[0])
        )
    {
        ModbusDataBlock *read_bk = FindBlock(readStartAddr, MBS_TYPE_HOLDING_REGISTER);
        ModbusDataBlock *write_bk = FindBlock(writeStartAddr, MBS_TYPE_HOLDING_REGISTER);
        if((read_bk != NULL) && (write_bk != NULL)) // check address
        {
            if(read_bk->addr + read_bk->quantity - readStartAddr - readQuantity >= 0)
            {
                if(write_bk->addr + write_bk->quantity - writeStartAddr - writeQuantity >= 0)
                {
                    uint8_t bytecount = readQuantity << 1; // read byte count
                    buffer_out[0] = bytecount;
                    uint16_t baseoff = readStartAddr - read_bk->addr;
                    uint16_t* dt = (uint16_t*)((uint32_t)(read_bk->pData) + (baseoff << 1));
                    for(int i = 0; i < bytecount; i = i + 2)
                    {
                        uint16_t value = *dt++;
                        buffer_out[i + 1] = (value & 0xFF00) >> 8;
                        buffer_out[i + 2] = (value & 0xFF);        // Modbus Standard is Big-endian
                    }

                    bytecount = buffer_in[0]; // write byte count
                    baseoff = writeStartAddr - write_bk->addr;
                    dt = (uint16_t*)((uint32_t)(write_bk->pData) + (baseoff << 1));
                    for(int i = 0; i < bytecount; i = i + 2)
                    {
                        uint16_t value = (buffer_in[i + 1] << 8) | buffer_in[i + 2]; // Modbus standard is Big-endian
                        *dt++ = value;
                    }
                    return MBS_ERR_OK;
                }
            }
            return MBS_ERR_ADDRESS;
        }
        else
        {
            return MBS_ERR_ADDRESS;
        }
    }
    else
    {
        return MBS_ERR_QUANTITY;
    }
}