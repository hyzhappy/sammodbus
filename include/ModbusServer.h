#pragma once
#include "ModbusData.h"

typedef mbs_err_t(*mbs_func_t)(uint8_t*, ModbusData*, uint8_t*, uint16_t*);

class ModbusServer
{
protected:
    uint8_t _slaveAddr;
    mbs_func_t _func[128]; // 0-127 function code table
    ModbusData* _dataModel;
public:
    ModbusServer();
    void SetSlaveAddress(uint8_t addr);
    void RegisterFunction(uint8_t code, mbs_func_t f);
    void SetDataModel(ModbusData* data);
    virtual int Serve(uint8_t* data_in, uint16_t length, uint8_t* data_out) = 0;
    static mbs_err_t func_0x01_std(uint8_t* pdu, ModbusData* model, uint8_t* data_out, uint16_t* length);
    static mbs_err_t func_0x02_std(uint8_t* pdu, ModbusData* model, uint8_t* data_out, uint16_t* length);
    static mbs_err_t func_0x03_std(uint8_t* pdu, ModbusData* model, uint8_t* data_out, uint16_t* length);
    static mbs_err_t func_0x04_std(uint8_t* pdu, ModbusData* model, uint8_t* data_out, uint16_t* length);
    static mbs_err_t func_0x05_std(uint8_t* pdu, ModbusData* model, uint8_t* data_out, uint16_t* length);
    static mbs_err_t func_0x06_std(uint8_t* pdu, ModbusData* model, uint8_t* data_out, uint16_t* length);
    static mbs_err_t func_0x0f_std(uint8_t* pdu, ModbusData* model, uint8_t* data_out, uint16_t* length);
    static mbs_err_t func_0x10_std(uint8_t* pdu, ModbusData* model, uint8_t* data_out, uint16_t* length);
    static mbs_err_t func_0x17_std(uint8_t* pdu, ModbusData* model, uint8_t* data_out, uint16_t* length);
};

class ModbusTCPServer : public ModbusServer
{
public:
    int Serve(uint8_t* data_in, uint16_t length, uint8_t* data_out) override;
};

class ModbusRTUServer : public ModbusServer
{
public:
    int Serve(uint8_t* data_in, uint16_t length, uint8_t* data_out) override;
};

