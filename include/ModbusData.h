#pragma once
#include "mbs_port.h"
#include <stdint.h>
#include <stdlib.h>
#define MBS_ERR_MASK   0x80
#define MBS_MAX_BIT_READ_QUANTITY             ((uint16_t)0x7D0)
#define MBS_MAX_BIT_WRITE_QUANTITY            ((uint16_t)0x7B0)
#define MBS_MAX_REGISTER_READ_QUANTITY        ((uint16_t)0x7D)
#define MBS_MAX_REGISTER_WRITE_QUANTITY       ((uint16_t)0x7B)
#define MBS_MAX_REGISTER_READ_WRITE_QUANTITY  ((uint16_t)0x79)

typedef enum
{
    MBS_ERR_OK = 0U,
    MBS_ERR_FUNCTION_CODE = 1U,
    MBS_ERR_ADDRESS = 2U,
    MBS_ERR_QUANTITY = 3U
}mbs_err_t;

typedef enum
{
    MBS_TYPE_COIL,              // Coil is read/write bits
    MBS_TYPE_INPUT_STATUS,      // input status is read only bits
    MBS_TYPE_INPUT_REGISTER,    // input register is read only registers
    MBS_TYPE_HOLDING_REGISTER   // holding register is read/write 16-bit registers
}mbs_data_type_t;

typedef struct
{
    uint16_t addr;
    uint16_t quantity;
    void* pData;
    mbs_data_type_t type;
}ModbusDataBlock;

class ModbusData
{
private:
    int _blockCount;
    ModbusDataBlock _blocks[MODBUS_MAX_BLOCKS];
private:
    ModbusDataBlock* FindBlock(uint16_t addr, mbs_data_type_t type);
    mbs_err_t ReadRegister(uint8_t* buffer_out, uint16_t startAddr, uint16_t quantity, mbs_data_type_t type);
    mbs_err_t ReadBit(uint8_t* buffer_out, uint16_t startAddr, uint16_t quantity, mbs_data_type_t type);
public:
    ModbusData();
    inline int GetBlockCount() const
    {
        return _blockCount;
    }
    void AddBlock(uint16_t addr, uint16_t quantity, void *data, mbs_data_type_t);
    // Function Code 0x01: Read coils
    mbs_err_t ReadCoil(uint8_t* buffer_out, uint16_t startAddr, uint16_t quantity);
    // Function Code 0x02: Read input status
    mbs_err_t ReadInputStatus(uint8_t* buffer_out, uint16_t startAddr, uint16_t quantity);
    // Function Code 0x03: Read holding registers
    mbs_err_t ReadHoldingRegister(uint8_t* buffer_out, uint16_t startAddr, uint16_t quantity);
    // Function Code 0x04: Read input registers
    mbs_err_t ReadInputRegister(uint8_t* buffer_out, uint16_t startAddr, uint16_t quantity);
    // Function Code 0x05: Write single coil
    mbs_err_t WriteCoil(const uint8_t* buffer_in, uint16_t startAddr);
    // Function Code 0x06: Write single holding registers
    mbs_err_t WriteHoldingRegister(const uint8_t* buffer_in, uint16_t startAddr);
    // Function Code 0x0F: Write multiple coils
    mbs_err_t WriteCoil(const uint8_t* buffer_in, uint16_t startAddr, uint16_t quantity);
    // Function Code 0x10: Write multiple holding registers
    mbs_err_t WriteHoldingRegister(const uint8_t* buffer_in, uint16_t startAddr, uint16_t quantity);
    // Function Code 0x17: Read/Write multiple registers
    mbs_err_t ReadWriteHoldingRegister(uint8_t* buffer_out, uint16_t readStartAddr, uint16_t readQuantity, const uint8_t* data_in, uint16_t writeStartAddr, uint16_t writeQuantity);
};