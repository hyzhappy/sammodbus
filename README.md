# SamModbus Project

SamModbus is a C++ based Modbus library

## Features

* Modbus RTU/TCP Slave implementation

* Support Function code

  * 0x01: Read coils
  * 0x02: Read input status
  * 0x03: Read holding registers
  * 0x04: Read input registers
  * 0x05: Write single coil
  * 0x06: Write signle register
  * 0x0F: Write multiple coils
  * 0x10: Write multiple registers
  * 0x17: Read write multiple registers

* Support rewrite exist function code's functionality

* Support custom function code

## Example in use

```cpp

#include "ModbusData.h"
#include "ModbusServer.h"

int main()
{
    ModbusTCPServer tcpServer;
    ModbusData mbsData;
    // add two block data in mbsData, one start address is 0x1000,
    // quantity is 16, and type is holding register
    // one start address is 0x2000, quantity is 16, and type is coil
    mbsData.AddBlock(0x1000, 16, mbs_data, MBS_TYPE_HOLDING_REGISTER);
    mbsData.AddBlock(0x2000, 16, mbs_data_coils, MBS_TYPE_COIL);
    // register function code as supported function code, use default implementation
    tcpServer.RegisterFunction(0x03, ModbusServer::func_0x03_std);
    tcpServer.RegisterFunction(0x10, ModbusServer::func_0x10_std);
    tcpServer.RegisterFunction(0x01, ModbusServer::func_0x01_std);
    tcpServer.RegisterFunction(0x02, ModbusServer::func_0x02_std);
    tcpServer.RegisterFunction(0x04, ModbusServer::func_0x04_std);
    tcpServer.RegisterFunction(0x05, ModbusServer::func_0x05_std);
    tcpServer.RegisterFunction(0x06, ModbusServer::func_0x06_std);
    tcpServer.RegisterFunction(0x0F, ModbusServer::func_0x0f_std);
    // set slave address
    tcpServer.SetSlaveAddress(0x01);
    // bind the data model
    tcpServer.SetDataModel(&mbsData);
    uint8_t tcp_send_buffer[2048];
    uint8_t tcp_receive_buffer[2048];
    while(1)
    {
        int n_receive = RECEIVE_TCP_DATA(tcp_receive_buffer); // your port to receive tcp data
        int n_send = tcpServer.Serve(tcp_receive_buffer, length, tcp_send_buffer); // n_receive is how many bytes received into tcp_receive_buffer
        SEND_TCP_DATA(tcp_send_buffer, n_send); // your port to send tcp dataq
    }
}

```